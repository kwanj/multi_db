from django.db import models

class Language(models.Model):
	name		= models.CharField(max_length=40,null=False,blank=False)
	extension 	= models.CharField(max_length=4,null=True,blank=True)
	category	= models.CharField(max_length=200,null=True,blank=True)
	timestamp	= models.DateTimeField(auto_now_add=True)
	updated		= models.DateTimeField(auto_now=True)

	class Meta:
		ordering =['-updated', '-timestamp']


	def __str__(self):
		return self.name

