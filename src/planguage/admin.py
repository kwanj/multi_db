from django.contrib import admin
from .models import Language


class LanguageModelAdmin(admin.ModelAdmin):
	list_display 		= ['name','extension','timestamp','updated']
	list_display_links	= ['name']
	search_fields 	= ['name','extension']

	class Meta:
		model = Language


admin.site.register(Language, LanguageModelAdmin)

