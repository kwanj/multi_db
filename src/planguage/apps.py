from django.apps import AppConfig


class PlanguageConfig(AppConfig):
    name = 'planguage'
